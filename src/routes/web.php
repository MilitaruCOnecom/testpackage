<?php
Route::get('testpackage', function(){
    echo 'Hello from the testpackage package!';
});

Route::get('test-index', 'Militaruc\Testpackage\App\Http\Controllers\TestpackageController@index');

Route::get('add/{a}/{b}', 'Militaruc\Testpackage\App\Http\Controllers\TestpackageController@add');
Route::get('subtract/{a}/{b}', 'Militaruc\Testpackage\App\Http\Controllers\TestpackageController@subtract');