<?php
namespace Militaruc\Testpackage\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller as BaseController;

class TestpackageController extends BaseController
{
    public function index()
    {
        echo 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    }

    public function add($a, $b){
        $result = $a + $b;
        return view('testpackageviews::add', compact('result'));
    }

    public function subtract($a, $b){
        echo $a - $b;
    }
}