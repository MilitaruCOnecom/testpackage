<?php

namespace Militaruc\Testpackage\App\Providers;

use Illuminate\Support\ServiceProvider;

class TestpackageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '../../../routes/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // register controllers
        $this->app->make('Militaruc\Testpackage\App\Http\Controllers\TestpackageController');


        // register views
        $this->loadViewsFrom(__DIR__.'../../../resources/views', 'testpackageviews');
    }
}
